import React from 'react'; //importamos la clase react desde el paquete react
import ReactDOM from 'react-dom'; //importamos el renderisador de react que es el react dom desde el paquete react

import Creador from './creador.js'//importamos la clase creador que hagamos en nuestro archivo creador.js


let elemento=<Creador />;//creamos nuestro elemento
let contenedor=document.getElementById("root");//en la carpeta public hay un index.html donde tenemos una division que se llama root donde tengo  todos mis elementos

ReactDOM.render(elemento, contenedor);//que quiero y donde quiero renderizar
