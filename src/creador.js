import React from 'react';
import {Formik, Form, Field, ErrorMessage} from 'formik';//importamos la biblioteca formik para nuestrio formulario
import * as Yup from 'yup';//para validar los campos en el formulario
import {Container, Button} from 'react-bootstrap';//importamos un contenedor y un boton tipo bootsrap


class Creador extends React.Component{//en react todos los componentes extienden a la clase react component


    enviarForm(valores, acciones){

        console.log(valores);

        let datos={            
            id:valores.id,
            valor:valores.valor
        }

        fetch(
            'http://localhost:4000/insertar',
            {
                method:'POST',
                headers:{'Content-type':'application/json',
                          '':'Access-Control-Allow-Origin'},
                body:JSON.stringify(datos)
            }
        );
    };

    render(){//funcion que se encarga de decir que tiene el componente react

        let elemento=<Formik 
            initialValues={//es un objeto json que tiene un arreglo de objetos que son los diferentes valores que va a tener mi formulario
                {
                    id:'',
                    valor:''

                }
            }

            onSubmit={this.enviarForm}//esta funcion es la que ejecutaremos cuando querramos enviar el formulario

            validationSchema={ Yup.object().shape(//validacion del esquema utilizando nuestra biblioteca yup
                {
                    id: Yup.number().typeError('Debe ser un número').required('Obligatorio'),
                    valor: Yup.string().required('Campo es obligatorio')

                }
            )

            }

            >
                <Container className="p-3">  
                    <Form>
                        <div className="form-group">
                            <label htmlFor='id'>ID</label>
                            <Field name="id" type="text" className="form-control"/>
                            <ErrorMessage name="id" className="invalid-feedback"></ErrorMessage>
                        </div>
                        <div className="form-group">
                            <label htmlFor='valor'>Valor</label>
                            <Field name="valor" type="text" className="form-control"/>
                            <ErrorMessage name="valor" className="invalid-feedback"></ErrorMessage>
                        </div>
                        <div className="form-group">
                            <Button type="submit" className="btn btn-primary m-4">Aceptar</Button>
                            <Button type="reset" className="btn btn-secondary">Cancelar</Button>
                        </div>
                    </Form>
                </Container>
            </Formik>;

            return elemento;

    };



}

export default Creador;// exportamos la clase como modulo

